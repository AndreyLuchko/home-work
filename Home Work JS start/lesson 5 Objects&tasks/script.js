// Создайте объект криптокошелек. В кошельке должно храниться имя владельца,
// несколько валют Bitcoin, Ethereum, Stellar и в каждой валюте дополнительно
// есть имя валюты, логотип, несколько монет и курс на сегодняшний день.
// Также в объекте кошелек есть метод, при вызове которого, он принимает
// имя валюты и выводит на страницу информацию.
// "Добрый день, ... ! На Вашем балансе (Название валюты и логотип) 
// осталось N монет, если Вы сегодня продадите их, то получите ... грн."

const wallet = {
    userName: "Andrey",
    bitcoin: {
        logo: "<img style='width:20px; height:20px' src='https://s2.coinmarketcap.com/static/img/coins/64x64/1.png' alt='bitcoin' /> ",
        price: 40047.65,
        coins: 3
    },
    ethereum: {
        logo: " <img style='width:20px; height:20px' src='https://s2.coinmarketcap.com/static/img/coins/64x64/1027.png' alt='ethereum' /> ",
        price: 2947.94,
        coins: 10
    },
    stellar: {
        logo: " <img style='width:20px; height:20px' src='https://s2.coinmarketcap.com/static/img/coins/64x64/512.png' alt='stellar' />",
        price: 0.1855,
        coins: 500
    },
    show: function () {
        alert(`Добрый день, ${wallet.userName} !`);
        let chooseCoin = prompt("Выберите валюту для ознакомления?", "bitcoin-ethereum-stellar");

        const exchangeRate = 30.2;
        const exchange = (b, a) => {
            return b * a * exchangeRate;
        };

        if (chooseCoin === "bitcoin") {
            let coinPrice = wallet.bitcoin.price;
            document.write(`На Вашем балансе ${wallet.bitcoin.logo}${chooseCoin} осталось ${wallet.bitcoin.coins} монет, если Вы сегодня продадите их, то получите ${exchange(wallet.bitcoin.coins, coinPrice).toFixed(2)} грн.`);
        } else if (chooseCoin === "ethereum") {
            let coinPrice = wallet.ethereum.price;
            document.write(`На Вашем балансе ${wallet.ethereum.logo}${chooseCoin} осталось ${wallet.ethereum.coins} монет, если Вы сегодня продадите их, то получите ${exchange(wallet.ethereum.coins, coinPrice).toFixed(2)} грн.`);
        } else if (chooseCoin === "stellar") {
            let coinPrice = wallet.stellar.price;
            document.write(`На Вашем балансе ${wallet.stellar.logo}${chooseCoin} осталось ${wallet.stellar.coins} монет, если Вы сегодня продадите их, то получите ${exchange(wallet.stellar.coins, coinPrice).toFixed(2)} грн.`);
        }
    }
}
wallet.show();