// #1
// Сделайте функцию, которая принимает параметром число от 1 до 7, 
// а возвращает день недели на Украинском языке

/*

    let dayNumber;

    function dayOfWeek() {
        let dayNumber = parseInt(prompt("Введіть число від 1 до 7", "1-7"));
        let dayArray = ["Понеділок", "Вівторок", "Середа", "Четверг", "П'ятниця", "Субота", "Неділя"];

        if (dayNumber < 1 || dayNumber > 7) {
            alert("Вкажить число від 1 до 7");
            return;
        }else {
            return dayArray[dayNumber - 1];   
        }  
    };
    document.write(dayOfWeek (dayNumber));

*/

// #2
// Дана строка вида 'var_text_hello'. Сделайте из него текст 'VarTextHello'.



    let output = "";

    function textTransorm(a) {

        for (let i = 0; i < a.length; i++) {
            let letterInput = a[i];
            if (i == 0) {
                output += a[0].toUpperCase();
            } else if (letterInput == "_") {
                output += a[i + 1].toUpperCase();
                i++;
            } else {
                output += letterInput;
            }
        }
        return output;
    }
    document.write(textTransorm("var_text_hello"));



// #3
// Создайте функцию которая будет заполнять массив 
// 10-ю иксами с помощью цикла.

/*

    function fillArray(a, b) {
        let arr = [];
        for (let i = 0; i < b; i++) {
            arr.unshift(a);
            
        }
        return arr;
    }
    document.write(fillArray("x", 10));

*/

// # 4
// Создайте маасив на 50 элементов и заполните каждый элемент его 
// номером, не используя циклы выведите каждый нечетный элемент 
// в параграфе, а четный в диве.

/*
    function fillArray(b) {
        let arr = [];
        for (let i = 0; i < b; i++) {
            arr.unshift(i);
            if (i % 2 == 0) {
                document.write("<div class='block'>" + i + "</div>");
            } else {
                document.write("<p class='par'>" + i + "</p>");
            }
            
        }
        return arr;
    }
    fillArray(50);
*/

// # 5
// Напиши функцию map(fn, array), которая принимает на вход функцию 
// и массив, и обрабатывает каждый элемент массива этой функцией, 
// возвращая новый массив.

/*
    const double = (x) => {
        return x * 2;
    }
    let arr = [1, 8, 14, 15, -45];


    function map(fn, array) {
        let newArray = [];
        for (let i = 0; i < array.length; i++) {
            const element = array[i];

            newArray.push(fn(element));
        }
        return newArray;
    }
    document.write(map(double, arr));
*/


// # 6
// Перепишите функцию, используя оператор '?' или '||'
// Следующая функция возвращает true, если параметр age больше 18. 
// В ином случае она задаёт вопрос confirm и возвращает его результат.
// function checkAge(age) {
//     if (age > 18) {
//         return true;
//     } else {
//         return confirm('Родители разрешили?');
//     }
// }

/*

    function checkAge(age) {
        const a = age > 18 ? true : confirm('Родители разрешили?')
        return a;
    }
    document.write(checkAge(5));

*/

// # 7
// Функция ggg принимает 2 параметра: анонимную функцию, 
// которая возвращает 3 и анонимную функцию, которая возвращает 4. 
// Верните результатом функции ggg сумму 3 и 4.

/*

    const a = (x) => {
        return 3
    };
    const b = (y) => {
        return 4
    };
    const ggg = (j, h) => {
        return j + h;
    }
    document.write(ggg(a(), b()));
*/


// # 8
// Если переменная a больше нуля - то в ggg запишем функцию, 
// которая выводит один !, иначе запишем функцию, которая выводит 
// два !


/*    

    function ggg(a) {
        const h = (x) => {return "!"};
        const j = (x) => {return "!!"};
        const b = a > 0 ? h() : j();
        document.write (b);
    }
    ggg(-6);
*/


// # 9
// Используя CallBack function создайте калькулятор, который
// будет от пользователя принимать 2 числа и знак арифметической 
// операции. При вводе не числа или при делении на 0 выводить ошибку.

/*

    function calc() {

        let numberOne = parseInt(prompt("Ведите первое число")) ;
        let operation = prompt("Введите знак операции, которую хотите произвести", "+ - * /");
        let numberTwo = parseInt(prompt("Введите второе число")) ;
        let result;

        function add(a, b) {
            return a + b;
        }

        function subtraction(a, b) {
            return a - b;
        }

        function multiplication(a, b) {
            return a * b;
        }

        function division(a, b) {
            return a / b;
        }
        if (isNaN(numberTwo) ||  isNaN(numberOne)) {
            alert("Данные не являются числом!");
        } else {
            if (operation == "+") {
                result = add(numberOne, numberTwo);
            } else if (operation == "-") {
                result = subtraction(numberOne, numberTwo);
            } else if (operation == "*") {
                result = multiplication(numberOne, numberTwo);

            } else {
                if (operation == "/" && numberTwo !== 0) {
                    result = division(numberOne, numberTwo);
                } else {
                    console.error("На ноль делить нельзя!");
                }
            }
        }

        document.write(` ${numberOne} ${operation} ${numberTwo} = ${result} `);

    }
    calc();

    */