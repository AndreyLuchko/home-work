// Home work JS lesson 2

// exercise 1 :Если переменная a равна 10, 
// то выведите 'Верно', иначе выведите 'Неверно'.

/*
    let a = prompt ("Введите число");

    if (a === "10") {
        document.write("Верно");
        
    } else {
        document.write("Не верно");
    };
*/

// exercise 2 :В переменной min лежит число от 0 до 59. 
// Определите в какую четверть часа попадает это число 
// (в первую, вторую, третью или четвертую).

/*
    let min = prompt("Введите число от 0 до 59", "0-59");

    if (min >= 0 && min <= 15) {
        document.write("Это 1 четверь часа");
    } 
    else if (min > 15 && min <= 30) {
        document.write("Это 2 четверть часа");
    }
    else if (min > 30 && min <= 45) {
        document.write("Это 3 четверть часа");
    }
    else if (min > 45 && min <= 59) {
        document.write("Это 4 четверть часа");
    }
    else {
        document.write ("Не верное число.")
    };
*/

// exercise 3 : Переменная num может принимать 4 значения: 1, 2, 3 или 4. 
// Если она имеет значение '1', то в переменную result запишем 'зима',
// если имеет значение '2' – 'весна' и так далее.

/*
    const num = prompt("Введите число соответствующее времени года", "1-2-3-4");
    let result;

    switch (num) {
        case "1":
        {result = "Зима"
            document.write("Это " + result);
            break;
        };    
            
        case "2":
        { result = "Весна"
            document.write("Это " + result);
            break;
        };
        
        case "3":
        {result = "Лето"
            document.write("Это " + result);
            break;
        };
            
        case "4":
        {result = "Осень"
            document.write("Это " + result);
            break;
        };
            
        default:
        {        
            document.write("Вы ввели неверное число. Попробуйте еще раз!");
        };
            break;
    };
*/

// exercise 4 : Прямоугольник

/*

    for (let i = 0; i < 10; ++i) {

        if (i == "0" || i == "9") {
            for (let j = 0; j < 10; ++j) {
                document.write("$");
            }
        }else {
            document.write("$");
            for (let j = 0; j < 8; ++j) {
                document.write("&ensp;");
            }
            document.write("$");
        }
        document.write("<br/>");
    };
*/


// exercise 5 : Прямоугольный треугольник

/*
    for (let i = 0; i < 10; ++i) {

        if (i == "0") {
            document.write("$");
        } else if (i == "9") {
            for (let j = 0; j < 10; ++j) {
                document.write("$");
            }
        } else {
            document.write("$");
            for (let j = 0; j < (i - 1); ++j) {
                document.write("&ensp;");
            }
            document.write("$");
        }
        document.write("<br/>");
    };
*/

// exercise 6 : Равносторонний треугольник

let xNum = 5;
let yNum = 4;
let str = 1;

for (let i = 0; i < xNum; i++) {
    for (let j = 0; j < yNum; j++) {
        document.write("&nbsp;&nbsp;");
    }
    for (let h = 0; h < str; h++) {
        document.write("$");

    }
    yNum -= 1;
    str += 2;
    document.write("<br/>");
};